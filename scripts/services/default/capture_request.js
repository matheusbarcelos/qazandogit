import {check} from 'k6';
import http from 'k6/http';
import Utils from "../../utils/utils.js";

export default class CaptureService {
  get_coupon() {
    const url = `${Utils.captureUrl()}/default/v2/coupon?Active=true`
    const params = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    let response = http.get(url,params)
    check(response, {
      'is status 200': () => response.status == 200,
      'validate body': (response) => response.body.includes('coupons'),
      'body size': (response) => response.body.length > 0,
    });
  }

  get_category() {
    const url = `${Utils.captureUrl()}/default/v2/category`
    const params = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    let response = http.get(url,params)
    check(response, {
      'is status 200': () => response.status == 200,
      'validate body': (response) => response.body.includes('categoryId'),
      'body size': (response) => response.body.length > 0,
    });

  }
  get_partner() {
    const url = `${Utils.captureUrl()}/default/v2/partner?Active=true&pageSize=30&page=1&OrderBy=0`
    const params = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    let response = http.get(url,params)
    check(response, {
      'is status 200': () => response.status == 200,
      'validate body': (response) => response.body.includes('partnerId'),
      'body size': (response) => response.body.length > 0,
    });
  }
}


