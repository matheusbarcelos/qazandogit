import { sleep } from 'k6';
import { group } from 'k6';
import CaptureService from './services/default/capture_request.js';

export const options = {
  stages: [
    { duration: '15s', target: 50 },
    { duration: '30s', target: 100 },
    { duration: '15s', target: 150 },
    { duration: '30s', target: 50 },
  ],
  thresholds: {
    http_req_failed: ['rate<0.05'],
    http_req_duration: ['p(95)<800'],
  },
};

export default function () {
  let capture_service = new CaptureService();

  group('Listar Cupons na Capture', () => {
    capture_service.get_coupon();
    sleep(1);
  });

  group('Listar Categorias na Capture', () =>{
    capture_service.get_category();
    sleep(1);
  })

  group('Listar Parceiros na Capture', () =>{
    capture_service.get_partner();
    sleep(1);
  })
}

// alteração branch

